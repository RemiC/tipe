import matplotlib.pyplot as plt
import Rame as R

#Initialisation de la liste des stations par abscisses croissantes
abs = [0, 1600, 2740, 3711, 4542, 5233, 5783, 6730, 7330, 7692, 8192, 8832, 10332, 11310, 11934]
interstations = [1600, 1140, 971, 831, 691, 550, 947, 600, 362, 500, 640, 1500, 978, 624]
tpsArret = [33, 18, 18, 17, 27, 23, 29, 24, 44, 26, 44, 28, 37, 23, 26]

def trapeze(distance):
    '''Trace un trapeze de vitesse pour une distance donnee.'''
    rame = R.Rame(distance)
    rame.ProfilVitesse()
    plt.plot(rame.Temps, rame.Vx)
    plt.xlabel("Temps (en s)")
    plt.ylabel("Vitesse (en m/s)")
    plt.title("Profil de vitesse en fonction du temps")
    plt.show()
    return(rame.Temps, rame.Vx)
    
def traceLigne(debut, fin):
    '''Trace le profil de vitesse et la position d'une rame sur l'ensemble de la ligne.'''
    V = []
    T = []
    P = []
    for i in range(tpsArret[debut]):
        T.append(i)
        V.append(0)
    for i in range(debut, fin):
        ts, vs = trapeze(interstations[i])
        for j in range(len(ts)):
            ts[j] = ts[j] + int(T[-1])
        if i == 0:
            cadence = ts[-1] - tpsArret[0]
        
        horloge = ts[-1]
        for k in range(tpsArret[i]):
            ts.append(k+horloge)
            vs.append(0)
        V = V + vs
        T = T + ts
        
    p = 0
    n = len(T)
    for i in range(n):
        p = p + (V[i] * (T[i]-T[i-1]))
        P.append(p)

    f = open("Renomme moi.csv", "w")
    f.write("Temps; Position; Vitesse\n")
    for i in range(n):
        if i != 0 and i != n - 1 and int(T[i]) != int(T[i+1]):
            f.write(str(int(T[i]))+";"+str(int(P[i]))+";"+str(int(V[i]))+"\n")
    f.close()
    
    plt.plot(T, V)
    plt.xlabel("Temps (en s)")
    plt.ylabel("Vitesse (en m/s)")
    plt.title("Position en fonction du temps")
    plt.figure()
    plt.plot(T, P)
    plt.xlabel("Temps (en s)")
    plt.ylabel("Position (en m)")
    plt.title("Profil de vitesse en fonction du temps")
    plt.show()
    return(cadence, T[-1] - tpsArret[14])
    
def passHeure():
    '''Calcul le nombre de passagers transportes par heure en canton fixe.'''
    cadence, tpsTot = traceLigne(0, 14)
    nRames = int(3600 / cadence)
    capacite = 325 * nRames
    passaHeure = int(capacite * 3600 / tpsTot)
    print(str(passaHeure)+' Passagers par heure pour une rotation de '+str(nRames)+' rames.')