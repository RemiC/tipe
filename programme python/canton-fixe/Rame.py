class Rame:
    
    def __init__(self, PAF):
        self._vMax = 22
        self._pas = 0.030
        self._gdf = 1 #_M/s^2 deceleration fonctionnelle
        self._GFULIM = 1.8 #M/s^2 deceleration d'urgence
        self._aFonct = 1.47 #Acceleration fonctionnelle
        self.abscisse = 0
        self.PAF = PAF
        self.stationSuiv = 0
        self._longueur= 37 #longueur d'une rame en m.
        self.Tempo = 27
        self.Vx = []
        self.Temps = []
        self.Station = True
        self._Duree = 0

    def defDX(self):
        """renvoi un tableau de distance croissant en fonction du Point d'arret fonctionnel et du _pas."""
        DX = [int(self.PAF)]
        
        n = int(self.PAF // self._pas)
        
        if n > 1: #On verifie qu'on n'est pas precisemment sur le PAF.
            for i in range(n):  #On ignore deliberement la valeur n (le range de n va jusqu'a n-1) pour etre sur de ne pas depasser le PAF.
                dxn = DX[i] - self._pas
                if dxn < 0:
                    dxn = 0
                DX.append(dxn)
        else:
            DX = [0]
        return(DX)

    def ProfilVitesse(self):
        '''Cree un profil de vitesse trapezoidal entre deux stations.'''
        
        v1 = [0]
        t1 = [0]
        v = 0
        i = 0
        t = (2*self._pas/self._aFonct)**0.5
        while v < self._vMax:
            v = self._aFonct*t1[i]
            if v > self._vMax:
                v = self._vMax
            v1.append(v)
            t1.append(t1[i]+t)
            i += 1
        
        v3 = [0]
        t3 = [0]
        v = 0
        i = 0
        t = (2*self._pas/self._gdf)**0.5
        while v < self._vMax:
            v = self._gdf*t3[i]
            if v > self._vMax:
                v = self._vMax
            v3.append(v)
            t3.append(t3[i]+t)
            i += 1
        v3.reverse()
        
        dist = 1/2 * (self._aFonct*t1[-1]**2 + self._gdf*t3[-1]**2)
        v2 = [v1[-1]]
        t2 = [t1[-1]]
        n = int((self.PAF - dist)//self._pas)
        for i in range(n):
            v2.append(self._vMax)
            t = self._pas/self._vMax
            t2.append(t2[i]+t)
        self.Vx = v1 + v2 + v3
        
        for i in range(len(t1)):
            t1[i] = t1[i] + self._Duree
        for i in range(len(t3)):
            t3[i] = t3[i] + t2[-1]
        
        self._Duree = t3[-1]
        
        self.Temps = t1 + t2 + t3

    def profilStation(self):
        '''Cree un profil de vitesse nul pour le temps d'arret en station.'''
        for i in range(self.Tempo):
            self.Vx.append(0)
            for i in range(self.Tempo):
                self.Temps.append(i + self._Duree)
                self._Duree +=  1