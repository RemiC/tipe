class Rame:
    
    def __init__(self, PAF):
        self._vMax = 22
        self._pas = 0.030
        self._gdf = 1 #_M/s^2 deceleration fonctionnelle
        self._GFULIM = 1.8 #M/s^2 deceleration d'urgence
        self._aFonct = 1.47 #Acceleration fonctionnelle
        self._effv = [0, 33, 36, 40, 44, 50, 60, 70, 75]
        self._eff = [54750, 54750, 45000, 30000, 23000, 20000, 16000, 14000, 13000] # effort de traction en fonction de _effv
        self._EFFUMIN = 161350 #N
        self._M = 85300 #kg
        self._MIT = 6900 #kg (masses tournantes)
        self._BR = 0.116 #Retard resistance a l'avancement
        self._AR = 18.4 * 10**(-6) #incertitude resistance a l'avancement
        self.abscisse = 0
        self.PAF = PAF
        self.stationSuiv = 0
        self._Vt1 = 0
        self._longueur= 37 #longueur d'une rame en m.
        self.Tempo = 33
        self.Ordre = 1

    def Vitk(Vm):
        """Convertit une vitesse en _M/s en km/h."""
        return(Vm*3.6)
    
    def EFFTRAC(self, VX):
        '''Calcul de l'effort de traction par interpolation lineaire.'''
    
        v = Vitk(VX)
        if VX == 0:
            return(54750)
        elif v >= 75:
            return(13000)
        else:
            i = 0
            
            while v <= self._effv[-1-i]:
                i = i + 1
    
            j = 8 - i
            a = (self._eff[j+1] - self._eff[j])/(self._effv[j+1] - self._effv[j])
            b = self._eff[j] - a*self._effv[j]
            
            return(a*v+b)
    
    def defDX(self, PAF):
        """renvoi un tableau de distance croissant en fonction du Point d'arret fonctionnel et du _pas."""
        DX = [int(self.PAF)]
        
        n = int(self.PAF // self._pas)
        
        if n > 1: #On verifie qu'on n'est pas precisemment sur le PAF.
            for i in range(n):  #On ignore deliberement la valeur n (le range de n va jusqu'a n-1) pour etre sur de ne pas depasser le PAF.
                dxn = DX[i] - self._pas
                if dxn < 0:
                    dxn = 0
                DX.append(dxn)
        else:
            DX = [0]
        return(DX)
    
    def VFCMDNP(self, DX):
        """Renvoi une liste contenant les vitesses d'asservissements en fonction du _pas."""
        assert(type(DX) == list)
        VX = []
        i = 0
        for dx in DX:
            res = (2*self._gdf*dx)**0.5
            if res > 22:
                res = 22
            VX.append(res)
        return(VX)
        
    def Vt2(self):
        '''Renvoi la vitesse une seconde apres.'''
        
        DX = self.defDX(int(self.PAF))
        Vtheo = self.VFCMDNP(DX)
        
        if self._Vt1 + self._aFonct > Vtheo[0]:
            self._Vt1 = Vtheo[0]
        else:
            self._Vt1 = self._Vt1 + self._aFonct
        return(self._Vt1)
        
    def RA(self, VX):
        """Resistance a l'avencement."""
        V = Vitk(VX)
        res = - self._M * ( self._BR + self._AR + V**2)
        return(res)
    
    def GFU(self, VX):
        """Calcul de la deceleration maximale en freinage d'urgence."""
        
        res = EFFTRAC(VX) / (self._M + self._MIT) * 10 #*10 car calcul valable pour une jante. En FU on se place dans le cas 5 essieux freinant (10 jantes)
        
        if res > 1.8:
            res = 1.8
        
        return(res)
    
    def DAFU(VX):
        """Calcule la distance necessaire pour un freinage d'urgence en fonction de la vitesse."""
        gfu = GFU(VX)
        dist = (VX**2)/(2*gfu)
        
        return(dist)