import matplotlib.pyplot as plt
import Rame as R
import Station as St
import Circuit as Ci
import Carrousel as Car
import time as t

#Initialisation de la liste des stations par abscisses croissantes
abs = [0, 1600, 2740, 3711, 4542, 5233, 5783, 6730, 7330, 7692, 8192, 8832, 10332, 11310, 11934]
tpsArret = [33, 18, 18, 17, 27, 23, 29, 24, 44, 26, 44, 28, 37, 23, 26]

cir = Ci.Circuit()

for i in range(len(abs)):
    cir.ajouter(St.Station(abs[i], tpsArret[i]))

def traceCmd(nRames, Save = False):
    '''Calcul de la position et du PAF en fonction du temps. Renvoi la vitesse moyenne et le temps de parcours.'''
    secondes = 1200 + 50*nRames
    t1 = t.clock()
    car = Car.Carrousel()
    temps = []
    position = []
    paf = []
    
    car.ajouter()
    x = []
    pt = []
    for i in range(secondes):
        car.avancer(cir)
        temps.append(i)
        
        if i%40 == 0 and i > 0 and i <= 40*(nRames):
            car.ajouter()

        xi = [] #liste des positions de chaque rame pour une seconde.
        xj = [] #liste des PAF de chaque rame pour chaque seconde.
        for k in range(car.rang+1):
            xi.append(int(car.position(k)))
            xj.append(int(car.paf(k)))
        x.append(xi) #Liste des positions de chaque rame pour chaque seconde
        pt.append(xj) #Liste des points d'arrets de chaque rame pour chaque seconde.

    nRames = car.rang + 1
    
    for i in range(nRames):
        pos = []
        paf = []
        for j in range(secondes):
            if i < len(x[j]):
                pos.append(x[j][i])
                paf.append(pt[j][i])
            else:
                pos.append(0)
                paf.append(0)
        plt.plot(temps, pos,'-', label="Rame "+str(i))
        plt.plot(temps, paf,'-', label="PAF "+str(i))
        if i == nRames // 2 or nRames == 1:
            j = 0
            while pos[j] == 0:
                j += 1
            debut = j
            while pos[j] < 11867:
                j+= 1
            fin = j
            duree = (temps[fin]-temps[debut])
            vMoy = 11934/ duree
    if nRames == 1 and Save:
        f = open("Renomme moi.csv", "w")
        f.write("Temps; Position\n")
        for i in range(len(pos)):
            f.write(str(temps[i])+";"+str(pos[i])+"\n")
        f.close()
    plt.title("Position des rames en fonction du temps")
    plt.xlabel("Temps (en s)")
    plt.ylabel("Position (en m)")
    plt.legend()
    plt.show()
    del car
    t2 = t.clock()
    print(t2-t1)
    return (vMoy, duree)

def courbeOpti(nRamesMax):
    '''Trace le nombre de passagers par heure en fonction du nombre de rames en circulation.'''
    t1 = t.clock()
    passaHeure = []
    nRames = []
    f = open("Renomme moi.csv", "w")
    f.write("Nombre de rames; Passagers heures\n")
    for i in range(1, nRamesMax+1):
        print("Iteration Num "+str(i)+" sur "+str(nRamesMax))
        vCom, time = traceCmd(i)
        capacite = 325 * i
        passHeure = capacite * 3600 / time
        passaHeure.append(passHeure)
        nRames.append(i)
        f.write(str(i)+";"+str(passHeure)+"\n")
    plt.figure()
    f.close()
    plt.plot( nRames, passaHeure, "-")
    plt.title("Nombre de passagers transportes par heure en fonction du nombre de rames")
    plt.xlabel("Nombre de rames")
    plt.ylabel("Nombre de passagers par heure")
    plt.show()
    t2 = t.clock()
    print(t2-t1)
