class Circuit:
    def __init__(self):
        
        self.stations = []
    
    def ajouter(self, station):
        '''Ajoute une station.'''
        self.stations.append(station)
    
    def position(self, pos):
        '''Retourne la position par rapport a l'origine de la station.'''
        return(self.stations[pos].xAbscisse)
    
    def GetTempo(self, pos):
        '''Retourne le temps d'arret en station.'''
        return(self.stations[pos].tpsArret)