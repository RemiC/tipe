#invariants:
pas = 0.030
gdf = 1 #m/s^2 deceleration fonctionnelle
vmax = 20 #m/s
GFULIM = 1.8 #m/s^2
effV = [0, 33, 36, 40, 44, 50, 60, 70, 75]
eff = [54750, 54750, 45000, 30000, 23000, 20000, 16000, 14000, 13000] # effort de traction en fonction de effv
EFFUMIN = 161350 #N
M = 85300 #kg
MIT = 6900 #kg (masses tournantes)
BR = 0.116
AR = 18.4 * 10**(-6)

def Vitk(Vm):
    """Convertit une vitesse en m/s en km/h."""
    return(Vm*3.6)

def EFFTRAC(VX):
   
    v = Vitk(VX)
    if VX == 0:
        return(54750)
    elif v >= 75:
        return(13000)
    else:
        i = 0
        
        while v <= effV[-1-i]:
            i = i + 1

        j = 8 - i
        a = (eff[j+1] - eff[j])/(effV[j+1] - effV[j])
        b = eff[j] - a*effV[j]
        
        return(a*v+b)

#calcul de la vitesse fonctionnelle d'anti-collision

def defDX(PAF):
    """renvoi un tableau de distance croissant en fonction du Point d'arret fonctionnel et du pas."""
    assert(type(PAF) == int)
    DX = [PAF]
    
    n = int(PAF // pas)
    
    for i in range(n):  #On ignore deliberement la valeur n (le range de n va jusqu'a n-1) pour etre sur de ne pas depasser le PAF.
        dxn = DX[i] - pas
        if dxn < 0:
            dxn = 0
        DX.append(dxn)
    return(DX)

def VFCMDNP(DX):
    """Renvoi une liste contenant les vitesses d'asservissements en fonction du pas."""
    assert(type(DX) == list)
    VX = []
    i = 0
    for dx in DX:
        res = (2*gdf*dx)**0.5
        if res > 22:
            res = 22
        VX.append(res)
    return(VX)
    
def RA(VX):
    """Resistance a l'avencement."""
    V = Vitk(VX)
    res = - M * ( BR + AR + V**2)
    return(res)

def GFU(VX):
    """Calcul de la deceleration maximale en freinage d'urgence."""
    
    res = EFFTRAC(VX) / (M + MIT) * 10 #*10 car calcul valable pour une jante. En FU on se place dans le cas 5 essieux freinant (10 jantes)
    
    if res > 1.8:
        res = 1.8
    
    return(res)

def DAFU(VX):
    """Calcule la distance necessaire pour un freinage d'urgence en fonction de la vitesse."""
    gfu = GFU(VX)
    dist = (VX**2)/(2*gfu)
    
    return(dist)