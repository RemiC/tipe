import Rame as R

class Carrousel:
    
    def __init__(self):
        self.carrousel = []
        self.rang = 0
        self.debut = 0 #Enregistre l'indice du debut de carrousel[i].
    
    def ajouter(self):
        '''Ajoute une nouvelle rame a l'origine.'''
        
        if len(self.carrousel) != 0:
            self.rang += 1
            
            if self.carrousel[self.rang - 1].abscisse < 1600:
                self.carrousel.append(R.Rame(self.carrousel[self.rang - 1].abscisse))
        else:
            self.carrousel.append(R.Rame(1600))
    
    def position(self, num):
        '''Retourne la position d'une rame par rapport a l'origine.'''
        return self.carrousel[num].abscisse
    
    def setPosition(self, num, pos):
        '''Met a jour la position d'une rame par rapport a l'origine.'''
        self.carrousel[num].abscisse = pos
    
    def paf(self, num):
        '''Retourne le point d'arret (relatif a la position de la rame).'''
        return self.carrousel[num].PAF
    
    def avancer(self, circuit):
        '''Calcul la position de chaque rame toutes les secondes.'''
        
        i = self.debut
        while i <= self.rang:
            v = self.carrousel[i].Vt2()
            pos = self.position(i) + v
            self.setPosition(i, pos)
            
            if pos < circuit.position(self.carrousel[i].stationSuiv) + 19 and pos > circuit.position(self.carrousel[i].stationSuiv) - 19 and self.carrousel[i].Tempo != 0: #Si la rame est en station (19 = longueur rame / 2) et qu'il n'est pas temps de redemarrer.
                self.carrousel[i].PAF = 0
                self.carrousel[i].Tempo -= 1
                
                if self.carrousel[i].Tempo == 0:
                    if self.carrousel[i].stationSuiv < 14: #On met a jour la station suivante (la premiere si on est en bout de circuit.
                        self.carrousel[i].stationSuiv += 1
                        self.carrousel[i].Tempo = circuit.GetTempo(self.carrousel[i].stationSuiv) #On reinitialise la tempo.
                    else:
                        self.carrousel[i].stationSuiv = 14
            else:
                if i == 0 and pos <= 11934:
                    self.carrousel[i].PAF = circuit.position(self.carrousel[i].stationSuiv) - self.position(i)
                elif i != 0 and pos <= 11934:
                    if self.position(i-1) < circuit.position(self.carrousel[i].stationSuiv) + 19:
                        self.carrousel[i].PAF = self.position(i-1) - self.position(i) - 50
                    else:
                        self.carrousel[i].PAF = circuit.position(self.carrousel[i].stationSuiv) - self.position(i)
                else:
                    del self.carrousel[i]
            i += 1