# Simulateur du métro autonome de Lyon (ligne D)

Ce repository contient le rendu de mon travail de TIPE de 2017. Vous y trouverez notament un rapport succint sur ce travail, le diaporama de présentation et tout le code Python qui a été nécessaire pour générer les différents graphiques.

## Contenu de la simulation

Le code Python simule un trajet complet sur la ligne D du métro de Lyon. L'objectif est de faire varier le nombre maximum de métros mis en circulation pour mesurer l'influence de l'usage du canton mobile déformable sur le nombre de passagers transportés par heure. Les explications du principes sont dans les documents pdf.

## Licence

CC BY-NC (Licence Creative Commons Attribution - Pas d’Utilisation Commerciale 4.0 International)
